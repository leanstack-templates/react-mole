# React Mole
A minimalist, yet fully functional stack to kick start your SaaS idea.

What's included?

- React frontend
- Node Express API
- RBAC with Auth0
- Sqlite

Who is this project for?
- **Indie hackers:** launch and validate your MVPs fast
- **Freelance developers:** kickstart a rapid protype for your next clients
- **Bootcamp students:** build a real-world app to show your future bosses 

### Seeing it in action

Recipe and shopping list sharing app
- https://react-mole.demo.getkubernetes.com

Authentication is one of the most boring and time-consuming part when building a multi-users app.
- Email/password-based and Google login provided by Auth0.
- Protected routes available only to authenticated users.
- Role-based Access Control: conditionally render components based on user role(s).
- Public and protected API endpoints.
- Database (SQLite) migration with [Knex](http://knexjs.org/).
- **Bonus:** Dockerfile and Kubernetes deployment config included.

# Configure Auth0
TBD

# How to contribute
We welcome suggestions and pull requests.

# What is mole?
https://en.wikipedia.org/wiki/Mole_sauce

# Author
A project by https://Shtack.carrd.co