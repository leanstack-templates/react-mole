
exports.up = function(knex) {
    return knex.schema.createTable("shopping_lists", t => {
        t.increments("id");
        t.string("user_id").notNullable();
        t.string("list_name").notNullable();
        t.jsonb("list_data").notNullable();
        t.timestamp('created_at').defaultTo(knex.fn.now())
      });
};

exports.down = function(knex) {
    return knex.schema.dropTable("shopping_lists");
};
