exports.seed = function(knex) {
  // Deletes ALL existing entries
  return knex("shopping_lists")
    .truncate()
    .then(function() {
      // Inserts seed entries
      return knex("shopping_lists").insert([
        {
          user_id: "nacho",
          list_name: "chocolate cookies",
          list_data: JSON.stringify(["flour", "eggs", "sugar", "cocoa powder"]) 
        },
        {
          user_id: "mary",
          list_name: "Quinoa salad",
          list_data: JSON.stringify(["quinoa", "chickpeas", "1 cucumber", "1 onion", "bell pepper", "olive oil", "lemon juice", "garlic"]) 
        }
      ]);
    });
};
