const express = require("express");
const app = express();
const jwt = require("express-jwt");
const jwks = require("jwks-rsa");
const cors = require("cors");
const bodyParser = require("body-parser");
const jwtAuthz = require("express-jwt-authz");

import { get_all, add_list } from "./service";

var port = process.env.PORT || 8080;

var jwtCheck = jwt({
  secret: jwks.expressJwtSecret({
    cache: true,
    rateLimit: true,
    jwksRequestsPerMinute: 5,
    jwksUri: "https://react-mole.auth0.com/.well-known/jwks.json"
  }),
  audience: "https://react-mole-api",
  issuer: "https://react-mole.auth0.com/",
  algorithms: ["RS256"]
});

app.use(cors());
app.use(bodyParser.json());
app.use(
  bodyParser.urlencoded({
    extended: true
  })
);

/**
 * Public endpoint
 */
app.get("/", async (req, res) => {
  res.send({ status: "ok" });
});

/**
 * Public endpoint - get all shopping lists
 */
app.get("/all", async (req, res) => {
  try {
    res.json(await get_all());
  } catch (error) {
    console.log(error);
    res.status(500).send({ msg: "Error" });
  }
});

/**
 * Protected endpoint - create a new shopping list
 */
app.post(
  "/shopping-list",
  jwtCheck,
  jwtAuthz(["create:list"]),
  async (req, res) => {
    try {
      await add_list(req.body);
      res.status(201).send();
    } catch (error) {
      console.log(error);
      res.status(500).send({ msg: "Error" });
    }
  }
);

app.use(function(err, req, res, next) {
  if (err.status === 401) {
    res.status(401).send("Forbidden");
  }
  next();
});

app.listen(port, () => console.log(`Server listening on port ${port}`));
