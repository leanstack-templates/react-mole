import knex from "./db";

/**
 * Get all shopping lists
 */
export const get_all = async () => {
  return await knex.from("shopping_lists").select("*").limit(50);
};


/**
 * Add a new shopping list
 * @param json payload 
 */
export const add_list = async payload => {
  await knex("shopping_lists").insert({
    user_id: payload.user_id,
    list_name: payload.list_name,
    list_data: JSON.stringify(payload.list_data)
  });
};
