import React from "react";
import { Router, Route, Switch } from "react-router-dom";

import history from "./utils/history";
import { useAuth0 } from "./react-auth0-spa";

import PrivateRoute from "./components/PrivateRoute";
import NavBar from "./components/NavBar";
import Layout from "./components/Layout"

import Profile from "./pages/Profile";
import Home from "./pages/Home";
import AddNew from "./pages/AddNew";
import About from "./pages/About";


function App() {
  const { loading } = useAuth0();

  if (loading) {
    return <div>Loading...</div>;
  }

  return (
    <Layout>
      <Router history={history}>
        <header>
          <NavBar />
        </header>
        <Switch>
          <Route path="/" exact component={Home}/>
          <PrivateRoute path="/new" exact component={AddNew}/>
          <PrivateRoute path="/profile" component={Profile} />
          <Route path="/about" exact component={About}/>
        </Switch>
      </Router>
    </Layout>
  );
}

export default App;