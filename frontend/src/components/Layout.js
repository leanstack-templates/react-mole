import React from "react";

export default ({ children }) => {
  return (
    <div className="App">
      <div className="AppContent">{children}</div>
      <footer className="Footer">
        <div>
          A project by <a href="https://shtack.carrd.co">Shtack</a>
        </div>
        <a
          href="https://gitlab.com/leanstack-templates/react-mole"
          target="_new"
        >
          View source
        </a>
      </footer>
    </div>
  );
};
