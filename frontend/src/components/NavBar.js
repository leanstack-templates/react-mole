import React from "react";
import { Link } from "react-router-dom";

import { useAuth0 } from "../react-auth0-spa";
import {
  AppBar,
  Toolbar,
  Box,
  Button,
  withStyles
} from "@material-ui/core";
import logo from "../logo.png";

const NavBar = () => {
  const { isAuthenticated, loginWithRedirect, logout } = useAuth0();

  return (
    <div className="App-header">
      <CoolAppBar>
        <Toolbar>
          <Brand component={Link} to="/">
            <img src={logo} alt="React-mole full stack boilerplate" />
          </Brand>
          {!isAuthenticated && (
            <>
              <Button component={Link} to="/about">
                About
              </Button>
              <Button
                variant="contained"
                color="primary"
                onClick={() => loginWithRedirect({})}
              >
                Log in
              </Button>
            </>
          )}

          {isAuthenticated && (
            <>
              <Button component={Link} to="/">
                Home
              </Button>
              <Button component={Link} to="/about">
                About
              </Button>
              <Button component={Link} to="/profile">
                Profile
              </Button>
              <Button variant="outlined" color="secondary" onClick={logout}>
                Log out
              </Button>
            </>
          )}
        </Toolbar>
      </CoolAppBar>
    </div>
  );
};

const CoolAppBar = withStyles(theme => ({
  root: {
    backgroundColor: "#80cbc4",
    boxShadow: "none"
  }
}))(AppBar);

const Brand = withStyles(theme => ({
  root: {
    flexGrow: 1
  }
}))(Box);
export default NavBar;
