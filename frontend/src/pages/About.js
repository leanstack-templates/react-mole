import React from "react";

import { Typography } from "@material-ui/core";

const About = () => {
  return (
    <div>
      <Typography variant="h3">React Mole</Typography>
      <Typography variant="body1">
        <p>
          React-Mole is a fictional Recipe/Grocery checklist sharing app to show
          case our <b>minimalist full stack starter kit</b>.
        </p>
        <p>
          The project contains the bare essentials to help you quickly build a
          full scale multi-tenant SaaS.
        </p>
      </Typography>

      <Typography variant="h5">What's included?</Typography>
      <ul>
        <li>React frontend (create-react-app)</li>
        <li>Rest API (node.js express)</li>
        <li>SQLite database</li>
        <li>Email/password based login + Google login (by Auth0)</li>
      </ul>
      <Typography variant="h5">Time-saving features:</Typography>
      <ul>
        <li>Public and private routes.</li>
        <li>
          Conditionally render React components based on login state or role(s).
        </li>
        <li>How to protect API endpoints using Auth0 RBAC.</li>
        <li>Database migration and seeding.</li>
        <li>Bonus: Dockerfile and Kubernetes deployment config included.</li>
      </ul>
      <p>
        The project is open source (MIT license). Browse&nbsp;
        <a
          href="https://gitlab.com/leanstack-templates/react-mole"
          target="_new"
        >
          source code
        </a>{" "}
        on Gitlab.
      </p>
    </div>
  );
};

export default About;
