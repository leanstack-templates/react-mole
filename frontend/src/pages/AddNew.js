import React, { useState } from "react";

import { useAuth0 } from "../react-auth0-spa";
import { add_new } from "../utils/api";
import history from "../utils/history";
import { magic } from "../utils/grocery";

import {
  Typography,
  TextField,
  Button,
  Paper,
  Container
} from "@material-ui/core";

const AddNew = () => {
  const { loading, user } = useAuth0();
  const [name, setName] = useState("");
  const [list, setList] = useState(magic());

  if (loading || !user) {
    return <div>Loading...</div>;
  }

  const addNew = async e => {
    try {
      e.preventDefault();
      await add_new(user, { list_name: name, list_data: list });
      history.push("/");
    } catch (error) {
      //TODO: Handle error!
      console.log("Backend error");
    }
  };

  const makeNewList = () => setList(magic());

  return (
    <Container maxWidth="xs">
      <Paper>
        <Typography variant="h6">New shopping list</Typography>
        <form
          noValidate
          autoComplete="off"
          onSubmit={e => {
            e.preventDefault();
          }}
        >
          <TextField
            id="name"
            name="listName"
            value={name}
            placeholder="List name"
            onChange={e => {
              setName(e.target.value);
            }}
          />
          <Button variant="outlined" onClick={makeNewList}>
            give me another one
          </Button>
          <ul>
            {list.map(x => (
              <li key={x}>{x}</li>
            ))}
          </ul>

          <p>
            <Button
              variant="contained"
              color="secondary"
              type="submit"
              onClick={addNew}
            >
              Save
            </Button>
            <Button onClick={() => history.push("/")}>Cancel</Button>
          </p>
        </form>
      </Paper>
    </Container>
  );
};

export default AddNew;
