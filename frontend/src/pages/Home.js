import React, { useState, useEffect } from "react";

import history from "../utils/history";
import { useAuth0 } from "../react-auth0-spa";

import { get_all } from "../utils/api";

import { Card, Button, Box } from "@material-ui/core";

const Home = () => {
  const { user, isAuthenticated } = useAuth0();
  const [all, setAll] = useState([]);

  useEffect(() => {
    const fetchData = async () => {
      setAll(await get_all(user));
    };
    user && fetchData();
  }, []);

  return (
    <div className="home">
      <Box fontSize="h5.fontSize" color="text.secondary">Who's cooking?</Box>
      <Box display="flex" justifyContent="flex-end">
        <Button
          variant="contained"
          color="primary"
          disabled={!isAuthenticated}
          onClick={() => history.push("/new")}
        >
          Add shopping list
        </Button>
      </Box>
      <section>
        {all &&
          Array.isArray(all) &&
          all.map(item => (
            <Card className="recipe" key={item.id}>
              <Box fontSize="h6.fontSize">{item.list_name}</Box>
              <p>by: {item.user_id}</p>
              <p>created: {item.created_at}</p>
              {print_list(JSON.parse(item.list_data))}
            </Card>
          ))}
      </section>
    </div>
  );
};

const print_list = list =>
  list && Array.isArray(list) ? (
    <ul>
      {list.map((item, index) => (
        <li key={`${item}${index}`}>{item}</li>
      ))}
    </ul>
  ) : null;

export default Home;
