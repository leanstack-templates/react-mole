// src/components/Profile.js

import React from "react";

import { useAuth0 } from "../react-auth0-spa";
import { Typography } from "@material-ui/core";

const Profile = () => {
  const { loading, user, roles } = useAuth0();

  if (loading || !user) {
    return <div>Loading...</div>;
  }

  return (
    <div>
      <Typography variant="h3">Profile</Typography>
      <Typography variant="body1">
        <p>User email: {user.email}</p>
        <br></br>
        <hr></hr>
        <h5>Auth0 user data:</h5>
        <p> Roles: {JSON.stringify(roles, null, 2)}</p>
        <pre>{JSON.stringify(user, null, 2)}</pre>
      </Typography>
    </div>
  );
};

export default Profile;
