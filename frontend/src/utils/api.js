import axios from "axios";

export const client = axios.create({
  baseURL: process.env.REACT_APP_API_SERVER
    ? process.env.REACT_APP_API_SERVER
    : "http://localhost:8080"
});

export const add_new = async (user, data) => {
  const res = await client.post(
    "/shopping-list",
    { user_id: user.nickname, ...data },
    {
      headers: {
        Authorization: `Bearer ${user.apiAccessToken}`
      }
    }
  );

  return  Promise.resolve(res);
};

export const get_all = async user => {
  const res = await client.get("/all", {
    headers: {
      Authorization: `Bearer ${user.apiAccessToken}`
    }
  });

  return Promise.resolve(res.data);
};
