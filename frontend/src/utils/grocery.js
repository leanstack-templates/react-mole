export const essentials = [
  "eggs",
  "oatmeal",
  "juice",
  "coffee",
  "apples",
  "bananas",
  "potatoes",
  "carrots",
  "chocolate",
  "tortillas",
  "beer",
  "ice cream",
  "TP"
];

/**
 * Randomly generate a shopping list
 */
export const magic = () => {
  const min = getRndInteger(2, essentials.length);
  const _essentials = essentials.slice(0);
  const list = [];
  for (var i = 0; i < min; i++) {
    const randomPosition = getRndInteger(0, _essentials.length);
    list.push(_essentials[randomPosition]);
    _essentials.splice(randomPosition, 1);
  }
  return list;
};

const getRndInteger = (min, max) =>
  Math.floor(Math.random() * (max - min)) + min;
